package com.featherlike.feather.generator;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.featherlike.feather.generator.entity.Column;
import com.featherlike.feather.generator.entity.Table;

public interface IGenerator {

	Map<Table, List<Column>> getTableMapFromExcel();

	void generateAll(Map<Table, List<Column>> tableMap) throws IOException;

}