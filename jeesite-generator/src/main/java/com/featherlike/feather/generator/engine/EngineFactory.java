package com.featherlike.feather.generator.engine;

import java.util.Map;

import com.featherlike.feather.generator.config.ConfigProperties;
import com.featherlike.feather.generator.config.Constant;
import com.featherlike.framework.common.util.MapUtil;

public class EngineFactory {
	private static EngineFactory instance;
	private Map<String, ITemplateEngine> beanMap;

	private EngineFactory() {
		beanMap = MapUtil.newHashMap();
		String templatePath = ConfigProperties.getTemplateDir()
				+ Constant.FILE_SEPARATOR + ConfigProperties.getTemlateEngine();
		synchronized (this) {
			beanMap.put(Constant.FREEMARKER, new FreeMarkerImpl(templatePath));
			beanMap.put(Constant.VELOCITY, new VelocityImpl(templatePath));
		}
	}

	public static EngineFactory getInstance() {
		if (null == instance) {
			instance = new EngineFactory();
		}
		return instance;
	}

	public ITemplateEngine getTemplateEngine(String temlateEngine) {
		return beanMap.get(temlateEngine);
	}

}
